<?php

/**
 *
 * This file is part of DanskeBankMobileApi.
 * 
 * Copyright 2012, Ahmad Nazir Raja
 * 
 * DanskeBankMobileApi is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * DanskeBankMobileApi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with DanskeBankMobileApi.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

error_reporting( E_ALL );

// Database configuration
define( "DB_NAME", "danske" );
define( "DB_HOST", "localhost" );
define( "DB_USERNAME", "root" );
define( "DB_PASSWORD", "" );

require("./lib/ActiveRecord.class.php");
require("./generated/transactions.class.php");


require("./../danskebank.class.php");

/*
 * Get args and run script
 */

echo "\n";

$cprNum = 0;
$pin = 0;
$fromDate = "";

if ( isset( $argv[1] ) ) {
  $cprNum = $argv[1];
} else {
  echo "CPR Number not specified.\n";
  exit("\n");
}

echo 'Enter 4 digit Pin: ';

system( 'stty -echo' );
$pin = trim( fgets( STDIN, 6 ) ); // 4 digit + detection char + plus EOL
system( 'stty echo' );
echo "\n";

if ( !is_numeric( $pin ) || strlen( $pin ) !== 4 ) {
  echo "Pin for mobile banking is invalid.\n";
  exit("\n");
}

$fromDate = "";
if ( isset( $argv[2] ) ) {
  $fromDate = $argv[2];
  // Date format should be: day.month.year i.e. 02.12.99
  $dateParts = explode( ".", $fromDate );
  if ( !checkdate( $dateParts[1], $dateParts[0], $dateParts[2] ) ) {
    echo "Date provided is incorrect";
    exit("\n");
  }
}

$bank = new DanskeBank();
$loggedIn = $bank->login( $cprNum, $pin );
if ( $loggedIn ) {
  echo "Logged in..\n";
} else {
  exit("Unable to login\n");
}

$accounts = $bank->getAccounts();
$transactionObjects = $bank->getTransactions( $accounts[0], $fromDate );
$transactionsArray = array();

foreach ( $transactionObjects as $transaction ) {
  $t = new Transactions();

  $t->setReference( $transaction->getReference() );

  $t->setDescription( $transaction->getDescription() );

  $amount = $transaction->getAmount();
  $amount = preg_replace( "/\./", "", $amount );
  $amount = preg_replace( "/,/", ".", $amount );
  $amount = preg_replace( "/[^0-9-\.]/", "", $amount );
  $t->setAmount( $amount );

  $date = preg_replace( "/[^0-9\.]/", "", $transaction->getDate() );
  $dateArray = explode( ".", $date );
  $date = $dateArray[2] . "." . $dateArray[1] . "." . $dateArray[0];
  $t->setDate( $date );

  $t->setStatus( $transaction->getStatus() );

  $t->save();
}
