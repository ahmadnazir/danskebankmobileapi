<?php
class Transactions extends ActiveRecord { 

  protected $id;
  protected $reference;
  protected $description;
  protected $amount;
  protected $date;
  protected $status;

  public function getId() {
    return $this->id;  
  }
  public function setId( $id ) {
    $this->id = $id;  
  }
  public function getReference() {
    return $this->reference;  
  }
  public function setReference( $reference ) {
    $this->reference = $reference;  
  }
  public function getDescription() {
    return $this->description;  
  }
  public function setDescription( $description ) {
    $this->description = $description;  
  }
  public function getAmount() {
    return $this->amount;  
  }
  public function setAmount( $amount ) {
    $this->amount = $amount;  
  }
  public function getDate() {
    return $this->date;  
  }
  public function setDate( $date ) {
    $this->date = $date;  
  }
  public function getStatus() {
    return $this->status;  
  }
  public function setStatus( $status ) {
    $this->status = $status;  
  }
  protected static function getTableName() {
    return "Transactions";  
  }

}
?>
