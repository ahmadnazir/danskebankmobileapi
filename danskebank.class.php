<?php

/**
 *
 * This file is part of DanskeBankMobileApi.
 * 
 * Copyright 2012, Ahmad Nazir Raja
 * 
 * DanskeBankMobileApi is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * DanskeBankMobileApi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with DanskeBankMobileApi.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/*
 * @class DanskeBank
 *
 *   This class contains functionality for accessing the user's danske bank account.
 *   The user needs to provide the cpr number and the 4 digit service code to login.
 *
 *   Objective:
 *   ---------   
 *   Since there is no official API available, this class aims to give the programmer a
 *   friendly interface to access Danske Mobile Baking.
 *
 *   Structure of the Class:
 *   ----------------------
 *   The class is mainly divided into three types of functions:
 *   1. Access Functions ( Download Html Pages )
 *   2. Parse or Find Functions ( Find information in the pages )
 *   3. Public functions that are exposed to the programmer.
 *
 *   Requirement:
 *   -----------
 *   The user needs to be registered for Danske Mobile Banking.
 *   @todo: write more, the url and all.
 *
 * @todo
 *   Move to exceptions:
 *   The reason why we haven't implemented exceptions is for consistency reasons i.e. either
 *   use exceptions in all functions or not. The reason for 'not' using them is because in
 *   cases where the input isn't valid e.g. cpr num, pin, account number and we throw an
 *   exception, the sensitive information is also logged in the error log. This shouldn't
 *   happen.. this is the same problem with using PDO.. it has to be always caught. I don't
 *   want to impose that responsibility on the programmer using this api.
 */
class DanskeBank {

  const DANSKE_BANK_LINK = "https://mobil.danskebank.dk/XI";

  const ACCOUNT_NUMBER_LENGTH = 10;
  const SESSION_CODE_LENGTH = 12;
  const SERVICE_CODE_LENGTH = 4;
  const CPR_NUMBER_LENGTH = 10;

  private $linkHandler; // Curl Link Handler to fetch html pages
  private $parser; // Parser for the html pages to extract information

  private $cprNum;
  private $sessionCode;

  function __construct() {
    $this->linkHandler = curl_init( self::DANSKE_BANK_LINK );
  }

  function __destruct() {
    curl_close( $this->linkHandler );
  }

  /*
   * Validation Functions
   */

  public static function isValidCprNum( $cprNum ) {
    if ( !is_numeric( $cprNum ) ) {
      return false;
    }
    if ( strlen( $cprNum ) !== 10 ) {
      return false;
    }
    return true;
  }

  public static function isValidPin( $pin ) {
    if ( !is_numeric( $pin ) ) {
      return false;
    }
    if ( strlen( $pin ) !== DANSKEBANK::SERVICE_CODE_LENGTH ) {
      return false;
    }
    return true;
  }

  public static function isValidSessionCode( $sessionCode = "" ) {
    if ( trim( $sessionCode ) == "" ) {
      return false;
    }
    return true;
  }

  public static function isValidAccountNumber( $accountNumber ) {
    if ( !is_numeric( $accountNumber ) ) {
      return false;
    }
    if ( strlen( $accountNumber ) !== DANSKEBANK::ACCOUNT_NUMBER_LENGTH ) {
      return false;
    }
    return true;
  }

  /*
   * Setters and Getters
   */

  private function setCprNum( $cprNum ) {
    if ( !DANSKEBANK::isValidCprNum( $cprNum ) ) {
      return false;
    }
    $this->cprNum = $cprNum;
    return true;
  }

  private function setSessionCode( $sessionCode ) {
    if ( !DANSKEBANK::isValidSessionCode( $sessionCode ) ) {
      return false;
    }
    $this->sessionCode = $sessionCode;
    return true;
  }

  /*
   * ----------------
   * Access Functions
   * ----------------
   *
   * Access mobile banking pages.
   *
   */


  /*
   * Get the page after the user has successfully logged in.
   *
   * @param $cprNum Integer
   *   User's cpr number (10 digits)
   * @param $pin Integer
   *   User's 4 digit pin to access the accounts
   *
   * @return Boolean|String
   *   Success: Page Html
   *   Failure: FALSE
   *
   * Command Line:
   * curl https://mobil.danskebank.dk/XI 
   * -d WP=XAI -d WO=Logon -d WA=MBLogon -d gsSprog=EN -d gsBrand=DB
   * -d gsProdukt=XAS -d gsNextObj=Forside -d gsNextAkt=MBForside
   * -d gsSikSystem=DI -d txiCPR=xxxxxxxxxx -d gsLogon=xxxx
   */
  private function getLoginPage( $cprNum = 0, $pin = 0 ) {

    if ( !DANSKEBANK::isValidCprNum( $cprNum ) ) {
      return false;
    }

    if ( !DANSKEBANK::isValidPin( $pin ) ) {
      return false;
    }

    $postData = array(

      // Language
      "gsSprog" => "EN",
      "gsBrand" => "DB",

      // Constants
      "gsProdukt" => "XAS",
      "gsSikSystem" => "DI",

      // Control the Page
      "gsNextObj" => "Forside",
      "gsNextAkt" => "MBForside",

      // Only for Login
      "txiCPR" => $cprNum,
      "gsLogon" => $pin
    );

    curl_setopt( $this->linkHandler, CURLOPT_POST, 1 );
    curl_setopt( $this->linkHandler, CURLOPT_POSTFIELDS, $postData );
    curl_setopt( $this->linkHandler, CURLOPT_RETURNTRANSFER, $postData );

    $data = curl_exec( $this->linkHandler );
    // $data = file_get_contents( "loggedin.xml" );

    if ( $data ) {
      return $data;
    }
    return false;
  }


  /*
   *
   * Command Line:
   * curl https://mobil.danskebank.dk/XI -d gsSprog=EN -d gsBrand=DB -d WP=XAS -d WO=Konto
   * -d WA=KTList -d WSES=xxxxxxxxxxxx -d WAFT=xxxxxxxxxx
   */
  private function getAccountsPage() {

    if ( !$this->isLoggedIn() ) {
      return false;
    }

    $postData = array(
      // Language
      "gsSprog" => "EN",
      "gsBrand" => "DB",

      // User information
      "WSES" => $this->sessionCode, // Secret Code
      "WAFT" => $this->cprNum,   // Cpr Number

      // Control the Page
      "WP" => "XAS",
      "WO" => "Konto",
      "WA" => "KTList",
    );

    curl_setopt( $this->linkHandler, CURLOPT_POST, 1 );
    curl_setopt( $this->linkHandler, CURLOPT_POSTFIELDS, $postData );
    curl_setopt( $this->linkHandler, CURLOPT_RETURNTRANSFER, $postData );

    $data = curl_exec( $this->linkHandler );
    // $data = file_get_contents( "accounts.xml" );

    if ( $data ) {
      return $data;
    }
    return false;
  }

  /**
   * @param $accountNumber Integer
   * @param @index Integer
   *
   * https://mobil.danskebank.dk/XI?WP=XAS&WAFT=xxxxxxxxxx&WSES=xxxxxxxxxxxx&WO=Konto&WA=KBList&WCI=xxxxxxxxxx
   *
   */
  private function getTransactionsPage( $accountNumber, $index = 1, $fromDate = "" ) {

    if ( !$this->isLoggedIn() ) {
      return false;
    }

    if ( !DANSKEBANK::isValidAccountNumber( $accountNumber ) ) {
      return false;
    }

    if ( $index <= 0 ) {
      return false;
    }

    if ( $fromDate ) {
      // Date format should be: day.month.year i.e. 02.12.99
      $dateParts = explode( ".", $fromDate );
      if ( !checkdate( $dateParts[1], $dateParts[0], $dateParts[2] ) ) {
	return false;
      }
    }

    $postData = array(
      // Language
      "gsSprog" => "EN",
      "gsBrand" => "DB",

      // User information
      "WSES" => $this->sessionCode, // Secret Code
      "WAFT" => $this->cprNum,   // Cpr Number

      // Control the Page
      "WP" => "XAS",
      "WO" => "Konto",
      "WA" => "KBList",

      // Select Account
      "WCI" => $accountNumber,

      /*
       * For controlling the batch of records to fetch
       *
       * WLS:
       * - The index, it is a 4 digit integer left padded with zeros
       * - It determines where to start fetching the transactions
       *   e.g. 2 means fetch the latest transactions but exclude the very latest one
       *        similary, 3 means exclude the latest two ones.. etc
       *
       * WLP:
       * - Without this option, we can't select the batch of records, don't know why
       * - From our tests, it has three values:
       *   1. 'No'  : for Nothing presumably
       *   2. 'Op'  : Up
       *   3. 'Ned' : Down
       * - We just use the 'No' value as we don't rely on 'Op' and 'Ned'
       *
       */
      "WLS" => str_pad( (int) $index, 4, "0", STR_PAD_LEFT ), // "0001"
      "WLP" => "No",

      /*
       * Dates
       */
      "txiFraDato" => $fromDate
      // "txiTilDato" => ""
    );

    curl_setopt( $this->linkHandler, CURLOPT_POST, 1 );
    curl_setopt( $this->linkHandler, CURLOPT_POSTFIELDS, $postData );
    curl_setopt( $this->linkHandler, CURLOPT_RETURNTRANSFER, $postData );

    $data = curl_exec( $this->linkHandler );
    // $data = file_get_contents( "transactions.xml" );
    return $data;
  }

  /*
   * --------------------
   * Parse/Find Functions
   * --------------------
   *
   * Parse the data (html pages) and find required information.
   *
   */


  /*
   * Fetch the session code from the html acquired after the user has logged in. The
   * session code authenticates the user and is used as a POST param to load other pages.
   *
   * @param $data String
   *   The html fetched after the user has logged in.
   *
   * @return Boolean|String
   *   Failure: False
   *   Success: Secret code
   */
  private function findSessionCode( $data ) {
    $matches = array();

    $pretext = "WSES=";
    $codeRegexp = "\w{" . DANSKEBANK::SESSION_CODE_LENGTH . "}";
    $key = "code";
    $regexp = "/" . $pretext . "(?<" . $key . ">" . $codeRegexp . ")/";

    preg_match( $regexp, $data, $matches );

    if ( isset( $matches[ $key ] ) ) {
      return $matches[ $key ];
    }
    return false;
  }

  private function findAccounts( $data ) {
    $matches = array();

    $pretext = "WCI=";
    $codeRegexp = "\w{" . DANSKEBANK::ACCOUNT_NUMBER_LENGTH . "}";
    $key = "code";
    $regexp = "/" . $pretext . "(?<" . $key . ">" . $codeRegexp . ")/";

    preg_match_all( $regexp, $data, $matches );

    if ( isset( $matches[ $key ] ) ) {
      return $matches[ $key ];
    }
    return false;
  }

  private function findTransactions( $rawHtml ) {
    // Response array containing the transaction objects
    $transactions = array();

    // An array containing all the transactions, but they are not grouped.
    // todo: write more about ungrouped transactions
    // We need to order them into objects so that every key contains an object
    $ungroupedTransactions = array();

    $lines = preg_split( "/\n/", $rawHtml );
    foreach ( $lines as $line ) {
      if ( strpos( $line, "<ul class=\"list\"" ) > -1 ) {
	$strippedLine = strip_tags( $line,"<a>" );
	$filteredData = preg_split( "/(<a\shref=)|(<\/a)|(>)|(Date:)|(Amount:)|(Status:)/", $strippedLine  );
	// Remove the empty values
	foreach( $filteredData as $value ) {
	  $value = trim( $value );
	  if ( !empty( $value ) ) {
	    $ungroupedTransactions[] = $value;
	  }
	}
	break;
      }
    }
    // Make an array of transactions objects from the ungrouped transactions array.
    $size = count( $ungroupedTransactions );
    for ( $i = 0; $i < $size; $i += 5 ) {

      $transaction = new Transaction();

      // Get the unique reference
      $link = $ungroupedTransactions[$i];
      $linkParts = preg_split( "/WCI2=/", $link );
      $reference = substr( $linkParts[1], 0, 29 );

      $transaction->setReference( $reference );
      $transaction->setDescription( $ungroupedTransactions[$i+1] );
      $transaction->setDate( $ungroupedTransactions[$i+2] );
      $transaction->setAmount( $ungroupedTransactions[$i+3] );
      $transaction->setStatus( $ungroupedTransactions[$i+4] );

      $transactions[] = $transaction;
    }

    return $transactions;
  }

  /*
   * ----------------
   * Public Functions
   * ----------------
   */

  /*
   * Check if the user is logged in (Doesn't take into account if the session has
   * expired)
   *
   * @return Boolean
   */
  public function isLoggedIn() {
    if ( !DANSKEBANK::isValidCprNum( $this->cprNum ) ) {
      return false;
    }
    if ( !DANSKEBANK::isValidSessionCode( $this->sessionCode ) ) {
      return false;
    }
    return true;
  }


  /*
   * Login functionality.
   * On success, the function internally sets:
   * - The CPR number
   * - The session code which is used to access other pages.
   *
   * @param $cprNum Integer
   *   User's cpr number (10 digits)
   * @param $pin Integer
   *   User's 4 digit pin to access the accounts
   *
   * @return Boolean
   */
  public function login( $cprNum = 0, $pin = 0 ) {
    $data = $this->getLoginPage( $cprNum, $pin );
    $sessionCode = $this->findSessionCode( $data );
    if ( $sessionCode ) {
      $this->setCprNum( $cprNum );
      $this->setSessionCode( $sessionCode );
      return true;
    }
    return false;
  }

  public function getAccounts() {
    $data = $this->getAccountsPage();
    if ( !$data ) {
      return false;
    }
    $accounts = $this->findAccounts( $data );
    if ( is_array( $accounts ) ) {
      return $accounts;
    }
    return false;
  }

  /*
   * Get all the transactions for the last one year, or since the date specified
   *
   * @param $accountNumber Integer
   * @param $fromDate String [optional]
   *   The format should be: dd.mm.yyyy
   *   If no date is specified, the date from 1 year ago will be used.
   *
   * @return Array
   *   An array of the transaction objects.
   */
  public function getTransactions( $accountNumber, $fromDate = "" ) {

    if ( !$fromDate ) {
      $year = ( (int) date("y") )- 1;
      $fromDate = date("d.m.") . $year; 
    }

    $step = 5; // In our tests, each page gives us 5 transactions
    $index = 1;
    $allTransactions = array();
    do {
      $transactionsPage = $this->getTransactionsPage( $accountNumber, $index, $fromDate );
      // echo "Downloaded: " . $index . " transactions.. \n";

      $transactions = $this->findTransactions( utf8_encode( $transactionsPage ) );
      foreach( $transactions as $transaction ) {
	$allTransactions[] = $transaction;
      }
      $index += $step;
    } while( !empty( $transactions ) );
    return $allTransactions;
  }
}

class Transaction {

  /*
   * The properties of the transaction
   */
  private $reference;
  private $description;
  private $date;
  private $amount;
  private $status;

  function getReference() {
    return $this->reference;  
  }
  function setReference( $reference ) {
    $this->reference = $reference;  
  }

  function getDescription() {
    return $this->description;
  }

  function setDescription( $description = "" ) {
    $this->description = $description;
  }

  function getDate() {
    return $this->date;
  }

  function setDate( $date = "" ) {
    $this->date = $date;
  }

  function getAmount() {
    return $this->amount;
  }

  function setAmount( $amount = "" ) {
    $this->amount = $amount;
  }

  function getStatus() {
    return $this->status;
  }

  function setStatus( $status = "" ) {
    $this->status = $status;
  }
}

/*
$bank = new Danskebank();
// utf8_encode( file_get_contents( "transactions.xml" ) )
echo print_r( $bank->getTransactions( $accountNum ), true);
*/